/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2009-2016 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.obbo.ad_actionButton;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.Sqlc;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class BrowseDocument extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strTableId = vars.getRequiredStringParameter("inpadTableId");
      String strRecordId = vars.getRequiredStringParameter("inprecordId");
      String strDocBaseType = vars.getRequiredStringParameter("inpdocbasetype");
      String strWindowId = vars.getRequiredStringParameter("inpwindowId");
      String strTabId = vars.getRequiredStringParameter("inpTabId");
      printPage(response, vars, strWindowId, strTabId, strTableId, strRecordId, strDocBaseType);
    } else if (vars.commandIn("SAVE")) {
      String strTableId = vars.getRequiredStringParameter("inpadTableId");
      String strRecordId = vars.getRequiredStringParameter("inprecordId");
      String strDocBaseType = vars.getRequiredStringParameter("inpdocbasetype");
      BrowseDocumentData[] data = BrowseDocumentData.select(this, strTableId, strDocBaseType,
          vars.getClient());
      if (data == null || data.length == 0)
        bdError(request, response, "RecordError", vars.getLanguage());
      else {
        String inputName = "inp" + Sqlc.TransformaNombreColumna(data[0].columnname);
        String strWindowPath = Utility.getTabURL(data[0].adTabId, "R", true);
        if (strWindowPath.equals(""))
          strWindowPath = strDefaultServlet;
        printPageClosePopUp(response, vars, strWindowPath + "?" + "Command=DIRECT&" + inputName
            + "=" + strRecordId);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strWindowId,
      String strTabId, String strTableId, String strRecordId, String strDocBaseType)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: Browse Document");
    String[] discard = { "" };
    discard[0] = new String("helpDiscard");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/obbo/ad_actionButton/BrowseDocument", discard).createXmlDocument();
    xmlDocument.setParameter("windowId", strWindowId);
    xmlDocument.setParameter("tabId", strTabId);

    xmlDocument.setParameter("inpadTableId", strTableId);
    xmlDocument.setParameter("inprecordId", strRecordId);
    xmlDocument.setParameter("inpdocbasetype", strDocBaseType);

    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("question",
        Utility.messageBD(this, "StartProcess?", vars.getLanguage()));
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("help", "");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
