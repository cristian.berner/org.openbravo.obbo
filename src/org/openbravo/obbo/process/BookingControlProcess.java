/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2009-2016 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.obbo.process;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.hibernate.LockMode;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.security.OrganizationStructureProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.AcctProcessTemplate;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.FactLine;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Sequence;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationAcctSchema;
import org.openbravo.model.financialmgmt.calendar.Period;
import org.openbravo.model.financialmgmt.calendar.Year;
import org.openbravo.obbo.BookingControl;
import org.openbravo.obbo.SequenceYear;

public class BookingControlProcess implements AcctProcessTemplate {

  static Logger log4j = Logger.getLogger(BookingControlProcess.class);

  @Override
  public boolean execute(AcctServer acctServer, AcctSchema as, ConnectionProvider conn,
      Connection con, VariablesSecureApp vars) throws ServletException {

    try {
      OBContext.createOBContext(vars.getUser());
      OBContext.setAdminMode(true);
      final BookingControl bcs = OBProvider.getInstance().get(BookingControl.class);

      bcs.setId(SequenceIdData.getUUID());

      bcs.setNewOBObject(true);

      // Client information for Booking control
      bcs.setClient(OBDal.getInstance().get(Client.class, acctServer.AD_Client_ID));

      // Organization information for Booking control
      bcs.setOrganization(OBDal.getInstance().get(Organization.class, acctServer.AD_Org_ID));

      // Accounting Schema information for Booking control
      bcs.setAccountingSchema(OBDal.getInstance().get(
          org.openbravo.model.financialmgmt.accounting.coa.AcctSchema.class, as.m_C_AcctSchema_ID));

      final DocumentType docType = getBookingDocumentType(OBDal.getInstance().get(
          Organization.class, acctServer.AD_Org_ID));

      // Document Type information for Booking control
      bcs.setDocumentType(docType);

      // Period information for Booking control
      bcs.setPeriod(OBDal.getInstance().get(Period.class, acctServer.C_Period_ID));

      // Table information for Booking control
      bcs.setTable(OBDal.getInstance().get(Table.class, acctServer.AD_Table_ID));

      // Record ID information for Booking control
      bcs.setRecordID(acctServer.Record_ID);

      // Is Active
      bcs.setActive(true);

      // Base Doc Type
      bcs.setDocumentCategory(acctServer.DocumentType);

      // Browse Original Doc

      bcs.setBrowseDocumentProcess(true);

      // Description: to be defined

      // Description information from original invoice document
      bcs.setDescription(createDescription(acctServer));

      Sequence sequence = getSequence(
          OBDal.getInstance().get(Organization.class, acctServer.AD_Org_ID),
          OBDal.getInstance().get(
              org.openbravo.model.financialmgmt.accounting.coa.AcctSchema.class,
              as.m_C_AcctSchema_ID));

      if (sequence == null)
        sequence = getDocumentSequence(docType);

      if (sequence == null) {
        return returnError(acctServer, "@OBBO_UnableToRetrieveSequence@",
            "Couldn't retrieve any sequence!");
      }

      Year year = OBDal.getInstance().get(Period.class, acctServer.C_Period_ID).getYear();
      String strFiscalYear = year.getFiscalYear();
      SequenceYear sequenceYear = null;
      if (sequence.isObboResetPerYear()) {
        sequenceYear = getSequenceYear(sequence, year);
        if (sequenceYear == null)
          return returnError(acctServer, "@OBBO_UnableToCreateSequence@",
              "Couldn't create a new sequence!");
      }

      StringBuffer strDocumentNo = new StringBuffer();
      if (sequence.getPrefix() != null) {
        strDocumentNo.append(parse(sequence.getPrefix(), strFiscalYear));
      }
      Long lSequenceNumber = getNextSequenceNumber(sequence, year, sequenceYear);
      strDocumentNo.append(lSequenceNumber.toString());
      if (sequence.getSuffix() != null) {
        strDocumentNo.append(parse(sequence.getSuffix(), strFiscalYear));
      }
      bcs.setDocumentNo(strDocumentNo.toString());

      incrementSequenceNumber(sequence, sequenceYear);

      OBDal.getInstance().save(sequence);
      OBDal.getInstance().flush();

      bcs.setCreationDate(new java.sql.Date(System.currentTimeMillis()));
      bcs.setUpdated(new java.sql.Date(System.currentTimeMillis()));
      bcs.setCreatedBy(OBDal.getInstance().get(User.class, vars.getUser()));
      bcs.setUpdatedBy(OBDal.getInstance().get(User.class, vars.getUser()));
      bcs.setAmount(getAmount(acctServer, as));
      OBDal.getInstance().save(bcs);
      OBDal.getInstance().flush();
    } catch (final Exception e) {
      log4j.error(e);
      return returnError(acctServer, e.getMessage(), e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
    }
    return true;
  }

  private boolean returnError(AcctServer acctServer, String strText, String strLogText) {
    log4j.error(strLogText);
    OBError result = new OBError();
    result.setMessage(strText);
    result.setType("ERROR");
    acctServer.setMessageResult(result);
    return false;
  }

  /**
   * Returns the next available sequence number for the given sequence. If sequenceYear is not null,
   * then the entry in obbo_sequence_year
   * 
   * @param sequence
   * @param year
   * @param sequenceYear
   * @return
   */
  private Long getNextSequenceNumber(Sequence sequence, Year year, SequenceYear sequenceYear) {
    if (sequenceYear == null)
      return sequence.getNextAssignedNumber();
    else
      return sequenceYear.getNextAssignedNumber();
  }

  /**
   * Returns a record of obbo_sequence_year table, that belongs to the provided sequence and year.
   * If it doesn't exists one entry, it is created. If some error occured, a null is returned
   * 
   * @param sequence
   * @param year
   * @return
   */
  private SequenceYear getSequenceYear(Sequence sequence, Year year) {

    OBCriteria<SequenceYear> obcSeqYear = OBDal.getInstance().createCriteria(SequenceYear.class);
    obcSeqYear.add(Restrictions.eq(SequenceYear.PROPERTY_SEQUENCE, sequence));
    obcSeqYear.add(Restrictions.eq(SequenceYear.PROPERTY_YEAR, year));
    obcSeqYear.setLockMode(LockMode.PESSIMISTIC_WRITE);
    List<SequenceYear> lSeqYears = obcSeqYear.list();
    if (lSeqYears.size() > 1) {
      log4j.error("Error: more than one entry found in the OBBO_SEQUENCE_YEAR table, for sequence "
          + sequence.getName() + ", and year " + year.getFiscalYear());
      return null;
    } else if (lSeqYears.size() == 0) {
      return createYearSequence(sequence, year);
    } else
      return lSeqYears.get(0);
  }

  /**
   * Creates a new entry in obbo_year_sequence table, belonging to the year and sequence provided
   * 
   * @param sequence
   * @param year
   * @return
   */
  private SequenceYear createYearSequence(Sequence sequence, Year year) {
    try {
      final SequenceYear sequenceYear = OBProvider.getInstance().get(SequenceYear.class);
      sequenceYear.setYear(year);
      sequenceYear.setSequence(sequence);
      sequenceYear.setActive(true);
      sequenceYear.setNextAssignedNumber(sequence.getOBBOFirstAssignedNumberForTheYear());
      OBDal.getInstance().save(sequenceYear);
      OBDal.getInstance().flush();
      return sequenceYear;
    } catch (final Exception e) {
      log4j.error(e);
      return null;
    }
  }

  /**
   * Increments the NextAssignedNumber value of the sequence (or sequenceYear, if not null), with
   * the increment by value of the sequence
   * 
   * @param sequence
   * @param sequenceYear
   * @return
   */
  private void incrementSequenceNumber(Sequence sequence, SequenceYear sequenceYear) {
    Long lIncrement = sequence.getIncrementBy();
    if (sequence.isObboResetPerYear()) {
      sequenceYear.setNextAssignedNumber(sequenceYear.getNextAssignedNumber() + lIncrement);
      OBDal.getInstance().save(sequenceYear);
    } else {
      sequence.setNextAssignedNumber(sequence.getNextAssignedNumber() + lIncrement);
      OBDal.getInstance().save(sequence);
    }
  }

  // *********************************************************************************************************

  private Sequence getDocumentSequence(DocumentType docType) {
    final OBCriteria<Sequence> obSeq = OBDal.getInstance().createCriteria(Sequence.class);
    Sequence seq = docType.getDocumentSequence();
    if (seq != null) {
      obSeq.add(Restrictions.eq(Sequence.PROPERTY_ID, docType.getDocumentSequence().getId()));
      // This lock makes the select to be materialized using an SQL:
      // "select ... for update"
      // this way several concurrent requests of booking control sequence numbers will never be
      // assigned the same number
      obSeq.setLockMode(LockMode.PESSIMISTIC_WRITE);
      return obSeq.list().get(0);
    }
    return null;
  }

  /**
   * Parses a string. If "YYYY" substring is found, it's replaced by the fiscal year in which the
   * document will be posted
   * 
   * @param text
   *          text to be parsed
   * @param strFiscalYear
   *          fiscal year in which the document will be posted
   * @return string with the "YYYY" substring replaced by the fiscal year
   */
  private String parse(String text, String strFiscalYear) {
    if (text.contains("YYYY")) {
      return text.replace("YYYY", strFiscalYear);
    }
    return text;
  }

  /**
   * 
   * @param organization
   * @return
   */
  public DocumentType getBookingDocumentType(Organization organization) {
    try {
      OBContext.setAdminMode(true);
      // Query to find Document type for booking control
      final OBCriteria<DocumentType> obCriteria = OBDal.getInstance().createCriteria(
          DocumentType.class);
      obCriteria.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, "OBBO_BCS"));
      obCriteria.add(Restrictions.eq(DocumentType.PROPERTY_ORGANIZATION, organization));

      if (OBContext.getOBContext().isInAdministratorMode()) {
        obCriteria.setFilterOnReadableClients(false);
        obCriteria.setFilterOnReadableOrganization(false);
      }

      final List<DocumentType> docs = obCriteria.list();
      DocumentType docType = null;
      if (docs != null && docs.size() > 0)
        docType = docs.get(0);
      else {
        OrganizationStructureProvider os = new OrganizationStructureProvider();

        // Line needed for back ground process execution
        os.setClientId(organization.getClient().getId());

        String strParentOrg = os.getParentOrg(organization.getId());
        docType = getBookingDocumentType(OBDal.getInstance().get(Organization.class, strParentOrg));
      }

      return docType;
    } finally {
      OBContext.restorePreviousMode();

    }
  }

  /**
   * 
   * @param organization
   * @param acctSchema
   * @return
   */
  public Sequence getSequence(Organization organization,
      org.openbravo.model.financialmgmt.accounting.coa.AcctSchema acctSchema) {
    Sequence sequence = null;

    try {
      OBContext.setAdminMode(true);
      // Query to find Document type for booking control
      final OBCriteria<OrganizationAcctSchema> obCriteria = OBDal.getInstance().createCriteria(
          OrganizationAcctSchema.class);
      obCriteria.add(Restrictions.eq(OrganizationAcctSchema.PROPERTY_ORGANIZATION, organization));
      obCriteria.add(Restrictions.eq(OrganizationAcctSchema.PROPERTY_ACCOUNTINGSCHEMA, acctSchema));
      if (OBContext.getOBContext().isInAdministratorMode()) {
        obCriteria.setFilterOnReadableClients(false);
        obCriteria.setFilterOnReadableOrganization(false);
      }
      // This lock makes the select to be materialized using an SQL:
      // "select ... for update"
      // this way several concurrent requests of booking control sequence numbers will never be
      // assigned the same number
      obCriteria.setLockMode(LockMode.PESSIMISTIC_WRITE);

      final List<OrganizationAcctSchema> docs = obCriteria.list();

      if (docs != null && docs.size() > 0)
        sequence = docs.get(0).getOBBOSequence();
      else {
        OrganizationStructureProvider os = new OrganizationStructureProvider();
        // Line needed for back ground process execution
        os.setClientId(organization.getClient().getId());
        String strParentOrg = os.getParentOrg(organization.getId());
        sequence = getSequence(OBDal.getInstance().get(Organization.class, strParentOrg),
            acctSchema);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return sequence;
  }

  /**
   * 
   * @param acctServer
   * @return
   */
  public String createDescription(AcctServer acctServer) {
    try {
      OBContext.setAdminMode(true);
      String strDescription = "";
      if (acctServer.C_DocType_ID != null && !acctServer.C_DocType_ID.equals(""))
        strDescription = OBDal.getInstance().get(DocumentType.class, acctServer.C_DocType_ID)
            .getIdentifier();
      if (acctServer.DocumentNo != null && !acctServer.DocumentNo.equals(""))
        strDescription = (!strDescription.equals("")) ? strDescription + " - "
            + acctServer.DocumentNo : acctServer.DocumentNo;
      if (acctServer.Name != null && !acctServer.Name.equals(""))
        strDescription = (!strDescription.equals("")) ? strDescription + " - " + acctServer.Name
            : acctServer.Name;
      if (acctServer.DateAcct != null && !acctServer.DateAcct.equals(""))
        strDescription = (!strDescription.equals("")) ? strDescription + " - "
            + acctServer.DateAcct : acctServer.DateAcct;
      if (acctServer.C_BPartner_ID != null && !acctServer.C_BPartner_ID.equals(""))
        strDescription = (!strDescription.equals("")) ? strDescription + " - "
            + acctServer.C_BPartner_ID : acctServer.C_BPartner_ID;
      if (acctServer.C_Project_ID != null && !acctServer.C_Project_ID.equals(""))
        strDescription = (!strDescription.equals("")) ? strDescription + " - "
            + acctServer.C_Project_ID : acctServer.C_Project_ID;
      return strDescription;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * 
   * @param acctServer
   * @param as
   * @return
   */
  public BigDecimal getAmount(AcctServer acctServer, AcctSchema as) {
    if (acctServer == null || as == null)
      return null;
    BigDecimal Amount = new BigDecimal("0.00");
    for (int i = 0; i < acctServer.m_fact.length; i++)
      if (acctServer.m_fact[i] != null
          && acctServer.m_fact[i].getM_acctSchema().m_C_AcctSchema_ID.equals(as.m_C_AcctSchema_ID))
        Amount = getFactLinesAmount(acctServer.m_fact[i].getLines());
    return Amount;
  }

  /**
   * 
   * @param lines
   * @return
   */
  public BigDecimal getFactLinesAmount(FactLine[] lines) {
    BigDecimal Amount = new BigDecimal("0.00");
    for (int i = 0; i < lines.length; i++)
      Amount = Amount.add(new BigDecimal(lines[i].getM_AmtAcctDr()));
    return Amount;
  }

}
