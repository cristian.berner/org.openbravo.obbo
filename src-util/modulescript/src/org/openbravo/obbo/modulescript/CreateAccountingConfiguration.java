/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2016 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.obbo.modulescript;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.modulescript.ModuleScript;
import org.openbravo.modulescript.ModuleScriptExecutionLimits;
import org.openbravo.modulescript.OpenbravoVersion;

public class CreateAccountingConfiguration extends ModuleScript {
  private static final Logger log4j = Logger.getLogger(CreateAccountingConfiguration.class);

  @Override
  // Inserting:
  // 1) Accounting schema tables for new tables in the module
  // 2) Period control for newly added DocBaseTypes
  public void execute() {
    try {
      ConnectionProvider cp = getConnectionProvider();
      createAcctSchemaTables(cp);
      createPeriodControl(cp);
    } catch (Exception e) {
      handleError(e);
    }
  }

  void createAcctSchemaTables(ConnectionProvider cp) throws Exception {
    CreateAccountingConfigurationData[] data = CreateAccountingConfigurationData
        .selectAcctSchema(cp);
    final String TABLE_OBBO_BOOKING = "FB432E7C4A63487A8F2A872D14570840";
    for (int i = 0; i < data.length; i++) {
      boolean existInAcctSchema = CreateAccountingConfigurationData.selectTables(cp,
          data[i].cAcctschemaId, TABLE_OBBO_BOOKING);
      if (!existInAcctSchema) {
        CreateAccountingConfigurationData.insertAcctSchemaTable(cp.getConnection(), cp,
            data[i].cAcctschemaId, TABLE_OBBO_BOOKING, data[i].adClientId);
      }
    }
  }

  private void createPeriodControl(ConnectionProvider cp) throws Exception {
    CreateAccountingConfigurationData.insertPeriodControl(cp.getConnection(), cp);
  }

  @Override
  protected ModuleScriptExecutionLimits getModuleScriptExecutionLimits() {
    return new ModuleScriptExecutionLimits("0AE82B43254E4F89ABEFA67083310061", null,
        new OpenbravoVersion(3, 0, 0));
  }
}
